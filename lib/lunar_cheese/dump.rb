#coding: utf-8

class String

  # Dump the string to a format used by Don't Starve table dumps.
  # This will a) surround the string with quotation marks (") and b) escape any quotation marks which it contains.
  #
  # @example Dumping string
  #   puts 'Hello "John", if this truly is your name.'.to_lua
  #   => "Hello \"John\", if this truly is your name."
  #
  # @return [String] Dumped version of the string.
  def to_lua
    "\"#{ gsub(/"/, '\"') }\""
  end

end

class Fixnum

  # Dump the fixnum to a format used by Don't Starve table dumps.
  # This will simply call #to_s
  #
  # @example Dumping fixnum
  #   puts 21.to_lua
  #   => '21'
  #
  # @return [String] Dumped version of the fixnum.
  def to_lua
    to_s
  end

end

class Float

  # Dump the float to a format used by Don't Starve table dumps.
  # This will simply call #to_s
  #
  # @example Dumping float
  #   puts 21.5.to_lua
  #   => '21.5'
  #
  # @return [String] Dumped version of the float.
  def to_lua
    to_s
  end

end

class FalseClass

  # Dump 'false' to a format used by Don't Starve table dumps.
  #
  # @example Dumping 'false'
  #   puts false.to_lua
  #   => 'false'
  #
  # @return [String] 'false'
  def to_lua
    to_s
  end

end

class TrueClass

  # Dump 'true' to a format used by Don't Starve table dumps.
  #
  # @example Dumping 'true'
  #   puts true.to_lua
  #   => 'true'
  #
  # @return [String] 'true'
  def to_lua
    to_s
  end

end

class Symbol

  # Dump the symbol to a format used by Don't Starve table dumps.
  # Depending on the symbol this will return a different string:
  # If it consists of alphanumeric characters & underscores, its string value will be returned.
  # If it consists of an opening bracket, followed by any amount of characters except an opening or closing bracket, followed by a closing bracket, its string value will be returned.
  # Else its string value, surrounded by brackets, will be returned.
  #
  # @example Dumping alphanumeric + underscore symbol.
  #   puts :is_alive.to_lua
  #   => 'is_alive'
  #
  # @example Dumping symbol starting and ending with bracket.
  #   puts :'[3 * 2]'.to_lua
  #   => '[3 * 2]'
  #
  # @example Dumping non-alphanumeric symbol, not enclosed in brackets.
  # puts :'number one'
  # => '[number one]'
  #
  # @return [String] Dumped version of the symbol.
  def to_lua
    s = to_s
    if s =~ /^[a-zA-Z0-9_]+$/
      s
    elsif s=~ /^\[[^\[\]]+\]$/ # An opening bracket, followed by >=1 of anything except an opening or closing bracket, followed by a closing bracket.
      s
    else
      "[#{ s }]"
    end
  end

end

class Hash

  # Dump the Hash to a format used by Don't Starve table dumps.
  # The result will, roughly, contain of a comma-separated list of assignments, which are the result of calling #to_lua on each key and value, and joining them with an '=' sign.
  # One exemption are Fixnum keys - in those cases, merely the result of calling #to_lua on the value will be part of the result.
  # This is to make use of the implicit indices in a Lua table dump like '!{1,2,"foo","bar",3}'.
  #
  # @example Dumping array-like hash.
  #   puts { 0 => 'a', 1 => 'b', 2 => 'c' }.to_lua
  #   => {"a","b","c"}
  #
  # @example Dumping nested hash.
  #   puts { :name => { :first_name => 'John', :last_name => 'Madden' } }.to_lua
  #   => {name={first_name="John",last_name="Madden"}}
  #
  # @return [String] Dumped version of the hash.
  def to_lua

    results = each_pair.map do |key, value|
      if key.is_a? Fixnum
        value.to_lua
      else
        "#{key.to_lua}=#{value.to_lua}"
      end
    end

    "{#{ results.join(',') }}"
  end

end
