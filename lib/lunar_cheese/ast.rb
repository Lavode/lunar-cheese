#coding: utf-8

module LunarCheese

  module AST

    class IntegerLiteral < Treetop::Runtime::SyntaxNode
      def to_rb
        text_value.to_i
      end

      def clean_tree
        elements.delete_if { true }
      end
    end

    class FloatLiteral < Treetop::Runtime::SyntaxNode
      def to_rb
        text_value.to_f
      end

      def clean_tree
        elements.delete_if { true }
      end
    end

    class BooleanLit < Treetop::Runtime::SyntaxNode
      def to_rb
        text_value == 'true'
      end

      def clean_tree
        elements.delete_if { true }
      end
    end

    class StringLiteral < Treetop::Runtime::SyntaxNode
      def to_rb
        (@internal_text || text.text_value).gsub(/\\"/, '"')
      end

      def clean_tree
        # Cleaning the tree will remove the 'text' node, so in order for #to_rb to work we'll have to either
        # a) keep that node, b) use self.text_value and get rid of leading and closing quote or c) store the value somewhere.
        @internal_text = text.text_value
        elements.delete_if { true }
      end
    end

    class IdentifierLiteral < Treetop::Runtime::SyntaxNode
      def to_rb
        text_value.to_sym
      end

      def clean_tree
        elements.delete_if { true }
      end
    end

    class Assignment < Treetop::Runtime::SyntaxNode
      def to_rb
        { identifier.to_rb => value.to_rb }
      end
    end

    class LuaArray < Treetop::Runtime::SyntaxNode
      def to_rb
        index = 0
        other_values.elements.map(&:value).inject({ index => value.to_rb }) do |hash, value|
          index += 1
          hash.merge({ index => value.to_rb })
        end
      end
    end

    class LuaHash < Treetop::Runtime::SyntaxNode
      def to_rb
        other_assignments.elements.inject(assignment.to_rb) { |hash, comma_and_assignment| hash.merge(comma_and_assignment.assignment.to_rb) }
      end
    end

    class LuaTable < Treetop::Runtime::SyntaxNode
      def to_rb
        # Todo: Refactor this. Also, the grammar currently allows {,1,2,3} which wouldn't be correct.
        return {} if text_value == '{}'

        index = 0

        ([child] + other_children.elements.map(&:child)).inject({}) do |result, child|
          if child.is_a? Assignment
            result.update(child.to_rb)
          else
            result[index] = child.to_rb
            index += 1
          end
        
          result
        end

      end
    end

  end

end
