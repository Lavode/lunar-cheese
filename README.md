# Lunar Cheese
## Introduction
This gem allows you to convert between Ruby objects, and Lua table dumps as they are used by the game [Don't Starve](http://www.dontstarvegame.com/).

It consists of:

* A Treetop-based PEG grammar, to parse the Lua table dump into a Ruby object.
* Additions to Ruby core classes, to dump those objects back into the same format.

If you find any issues, create a ticket on the [issue tracker](https://bitbucket.org/Lavode/lunar-cheese/issues?status=new&status=open).

## Usage
Example usage, Ruby 1.9 & 2.0, parsing save game.

    #!/usr/bin/env ruby
    # coding: utf-8

    require 'zlib'
    require 'base64'
    require 'lunar_cheese'

    raw = IO::read('survival_1')
    # Base64-decoding, discarding the first eleven bytes
    decoded = Base64::decode64(raw[11..-1])
    # Inflating, discarding the first 16 bytes
    inflated = Zlib::inflate(decoded[16..-1])
    # Discarding the 'return' string at the beginning
    input = inflated[7..-1]

    parser = LunarCheese::LuaTableDumpParser.new
    ast = parser.parse(input)
    result = ast.to_rb
    puts result.inspect

Example usage, dumping Ruby structure

    #!/usr/bin/env ruby
    # coding: utf-8

    # Due to modifying Ruby core classes, you'll have to load this file manually.
    require 'lunar_cheese/dump'

    { :menu => { 1 => { 1 => :ham, 2 => :bacon }, 2 => { 1 => :spam, 2 => :egg, 3 => :bacon, 4 => :spam } } }.to_lua
    # => "{menu={{ham,bacon},{spam,egg,bacon,spam}}}"

## Compatability
This gem has been tested to work on Ruby 2.0.0-p0, 2.0.0-p247, Ruby 1.9.3-p448 and JRuby 1.7.4.

Note that performance on JRuby is worse by quite a bit. Probably related to [Treetop's constant usage of extend()](https://groups.google.com/forum/#!topic/treetop-dev/aujDDSZw368)

## Contributing

* Fork
* Feature branch
* Pull request / git-format-patch
