#coding: utf-8

Gem::Specification.new do |s|
    s.name = 'lunar_cheese'
    s.version = '0.2.4.pre'
    s.summary = 'Treetop grammar & additions to core classes to convert "Don\'t Starve" save games to Ruby and vice versa.'
    s.description = 'A gem consisting of two parts: A Treetop-based grammar which allows to parse save games of the game "Don\'t Starve" to Ruby objects, and additions to Ruby core classes to dump those back into the same format.'

    s.author = 'Michael Senn'
    s.email = 'morrolan@morrolan.ch'
    s.homepage = 'https://bitbucket.org/Lavode/lunar-cheese'
    s.license = 'Apache 2.0'

    s.files = `git ls-files`.split("\n")
    s.test_files = `git ls-files spec/`.split("\n")

    s.add_dependency 'treetop'

    s.add_development_dependency 'rspec'
end
