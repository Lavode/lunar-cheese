#coding: utf-8

require_relative '../lib/lunar_cheese'
require_relative '../lib/lunar_cheese/dump'

def parse(input)
  ast = p.parse(input)

  if ast.respond_to?(:to_rb)
    ast.to_rb
  else
    nil
  end
end
