#coding: utf-8

require_relative '../spec_helper'

module LunarCheese

  describe LuaTableDumpParser do
    let(:p) { LuaTableDumpParser.new }

    context 'parsing' do
      before(:each) { p.root = :integer }

      context 'valid integers' do

        it 'should parse basic integers' do
          expect(parse('0')).to eq 0
          expect(parse('21')).to eq 21
          expect(parse('928')).to eq 928
        end

        it 'should parse integers with a prefix' do
          expect(parse('+0')).to eq 0
          expect(parse('-0')).to eq 0
          expect(parse('+216')).to eq 216
          expect(parse('-31')).to eq -31
        end

      end

      context 'invalid integers' do

        it 'should not parse integers with more than one prefix' do
          expect(parse('--21')).to be_nil
          expect(parse('-+18')).to be_nil
          expect(parse('+-157')).to be_nil
          expect(parse('++24')).to be_nil
        end

        it 'should not parse integers with an invalid prefix' do
          expect(parse('*21')).to be_nil
          expect(parse('§18')).to be_nil
        end

        it 'should not parse integers with a prefix anywhere but the very beginning' do
          expect(parse('2+1')).to be_nil
          expect(parse('-14+5')).to be_nil
        end

        it 'should not parse prefixes alone' do
          expect(parse('-')).to be_nil
          expect(parse('+')).to be_nil
        end

        it 'should not parse integers which consist of non-numeric characters' do
          expect(parse('12a')).to be_nil
          expect(parse('a51§')).to be_nil
        end

      end

    end

    context 'dumping' do

      it 'should dump basic integers' do
        expect(0.to_lua).to eq '0'
        expect(928.to_lua).to eq '928'
      end

      it 'should dump integers with a prefix' do
        expect(-50.to_lua).to eq '-50'
        expect(-21.to_lua).to eq '-21'
      end

      it 'should not print the prefix for numbers >= 0' do
        expect(-0.to_lua).to eq '0'
        expect(0.to_lua).to eq '0'
        expect(+213.to_lua).to eq '213'
      end

    end

  end

end
