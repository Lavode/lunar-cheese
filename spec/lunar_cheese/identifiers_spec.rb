#coding: utf-8

require_relative '../spec_helper'

module LunarCheese

  describe LuaTableDumpParser do
    let(:p) { LuaTableDumpParser.new }

    context 'parsing' do
      before(:each) { p.root = :identifier }

      context 'valid identifiers' do

        it 'should parse identifiers' do
          expect(parse('x')).to eq :x
          expect(parse('age')).to eq :age
          expect(parse('last_name')).to eq :last_name
        end

        it 'should parse identifiers in brackets' do
          expect(parse('[1]')).to eq :'[1]'
          expect(parse('[25]')).to eq :'[25]'
          expect(parse('[*]')).to eq :'[*]'
          expect(parse('[liar liar pants on fire!]')).to eq :'[liar liar pants on fire!]'
        end

      end

      context 'invalid identifiers' do

        it 'should not parse non-alphanumeric (+underscore) identifiers without brackets' do
          expect(parse('ab*c')).to be_nil
          expect(parse('foo-bar_baz')).to be_nil
          expect(parse('Hello there. :D')).to be_nil
        end

      end

    end

    context 'dumping' do

      it 'should dump alphanumeric (+underscore) identifiers' do
        expect(:last_name.to_lua).to eq 'last_name'
        expect(:age.to_lua).to eq 'age'
      end

      it 'should dump non-alphanumeric (+underscore) identifiers' do
        expect(:'e=m*c^2'.to_lua).to eq '[e=m*c^2]'
        expect(:'a big horse'.to_lua).to eq '[a big horse]'
      end

      it 'should dump identifiers inside square brackets' do
        expect(:'[1]'.to_lua).to eq '[1]'
        expect(:'[liar liar pants on fire!]'.to_lua).to eq '[liar liar pants on fire!]'
      end

    end

  end

end
