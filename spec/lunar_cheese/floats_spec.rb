#coding: utf-8

require_relative '../spec_helper'

module LunarCheese

  describe LuaTableDumpParser do
    let(:p) { LuaTableDumpParser.new }

    context 'parsing' do
      before(:each) { p.root = :float }

      context 'valid floats' do

        it 'should parse basic floats' do
          expect(parse('0.512')).to eq 0.512
          expect(parse('21.27')).to eq 21.27
          expect(parse('0.00')).to eq  0
        end

        it 'should parse floats with a prefix' do
          expect(parse('-193.2')).to eq -193.2
          expect(parse('+9.1')).to eq 9.1
          expect(parse('-211.286')).to eq -211.286
        end

      end

      context 'invalid floats' do

        it 'should not parse floats with more than one period' do
          expect(parse('15..6')).to be_nil
          expect(parse('14.5.7')).to be_nil
        end

        it 'should not parse floats with a period in the beginning or end' do
          expect(parse('132.')).to be_nil
          expect(parse('.25')).to be_nil
        end

      end

    end

    context 'dumping' do

      it 'should dump basic floats' do
        expect(0.0.to_lua).to eq '0.0'
        expect(928.51.to_lua).to eq '928.51'
      end

      it 'should dump floats with a prefix' do
        expect(-50.2.to_lua).to eq '-50.2'
        expect(-21.5.to_lua).to eq '-21.5'
      end

      it 'should not print the prefix for numbers >= 0' do
        expect(-0.to_lua).to eq '0'
        expect(0.5.to_lua).to eq '0.5'
        expect(+213.21.to_lua).to eq '213.21'
      end

    end

  end

end
