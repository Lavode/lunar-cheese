#coding: utf-8

require_relative '../spec_helper'

module LunarCheese

  describe LuaTableDumpParser do
    let(:p) { LuaTableDumpParser.new }

    context 'parsing' do
      before(:each) { p.root = :assignment }

      context 'valid assignments' do

        it 'should parse assignments of numerics' do
          expect(parse('x=12')).to eq({ :x => 12 })
          expect(parse('foo=21.5')).to eq({ :foo => 21.5 })
          expect(parse('salary=9.5')).to eq({ :salary => 9.5 })
          expect(parse('[3]=15.345')).to eq({ :'[3]' => 15.345 })
        end

        it 'should parse assignments of strings' do
          expect(parse('race="Human"')).to eq({ :race => 'Human' })
          expect(parse('greeting="Hello World!"')).to eq({ :greeting => 'Hello World!' })
          expect(parse('[e=]="m*c^2"')).to eq({ :'[e=]' => 'm*c^2' })
        end

        it 'should parse assignments of booleans' do
          expect(parse('is_working=true')).to eq({ :is_working => true })
          expect(parse('on_fire=false')).to eq({ :on_fire => false })
          expect(parse('[x && true]=true')).to eq({ :'[x && true]' => true })
        end

        it 'should parse assignment of tables' do
          expect(parse('person={name="John",age=55}')).to eq({ :person => { :name => 'John', :age => 55 } })
          expect(parse('coords={10,5,10}')).to eq({ :coords => { 0 => 10, 1 => 5, 2 => 10 } })
          expect(parse('coords={foo="bar",1,2}')).to eq({ :coords => { :foo => 'bar', 0 => 1, 1 => 2 } })
        end

      end

      context 'invalid assignments' do

        it 'should not parse assignments which are missing the lhs' do
          expect(parse('=3')).to be_nil
        end

        it 'should not parse assignments which are missing the rhs' do
          expect(parse('foo=')).to be_nil
        end

        it "should not parse assignments without an 'equals' sign" do
          expect(parse('foo123')).to be_nil
        end

        it "should not parse assignments with multiple 'equals' signs" do
          expect(parse('foo=="bar"')).to be_nil
          expect(parse('foo="bar"="baz"')).to be_nil
        end

        it "should not parse the 'equals' sign alone" do
          expect(parse('=')).to be_nil
        end

      end

    end

  end

end
