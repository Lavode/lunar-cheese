#coding: utf-8

require_relative '../spec_helper'

module LunarCheese

  describe LuaTableDumpParser do
    let(:p) { LuaTableDumpParser.new }

    context 'parsing tables' do
      before(:each) { p.root = :table }

      context 'parsing' do

        context 'valid tables' do

          context 'flat tables' do

            it 'should parse empty tables' do
              expect(parse('{}')).to eq({})
            end

            it 'should parse array-like tables' do
              expect(parse('{1,2,3,4}')).to eq({ 0 => 1, 1 => 2, 2 => 3, 3 => 4 })
              expect(parse('{"Switzerland","Germany","France"}')).to eq({ 0 => 'Switzerland', 1 => 'Germany', 2 => 'France' })
              expect(parse('{false,false,true,false}')).to eq({ 0 => false, 1 => false, 2 => true, 3 => false })
            end

            it 'should parse hash-like tables' do
              expect(parse('{x=10}')).to eq({ :x => 10 })
              expect(parse('{name="John",age=25,salary=20.5,on_fire=true}')).to eq({ :name => 'John', :age => 25, :salary => 20.5, :on_fire => true })
            end

            it 'should parse mixed tables' do
              expect(parse('{pos="relative",10,5,20}')).to eq({ :pos => 'relative', 0 => 10, 1 => 5, 2 => 20 })
              expect(parse('{"a",[1]=5,6,foo="bar"}')).to eq({ 0 => 'a', :'[1]' => 5, 1 => 6, :foo => 'bar' })
            end

          end

          context 'nested tables' do

            it 'should parse array-like tables' do
              expect(parse('{1,{1.1,1.2},2}')).to eq({ 0 => 1, 1 => { 0 => 1.1, 1 => 1.2 }, 2 => 2 })
              expect(parse('{{29.2,119.2},{83.1,134}}')).to eq({ 0 => { 0 => 29.2, 1 => 119.2 }, 1 => { 0 => 83.1, 1 => 134 } })
            end

            it 'should parse hash-like tables' do
              expect(parse('{name={first_name="John",last_name="Madden"}}')).to eq({ :name => { :first_name => 'John', :last_name => 'Madden' } })
            end

            it 'should parse mixed tables' do
              expect(parse('{1,2,coords={3,4},5,foo="bar",{10,20}}')).to eq({ 0 => 1, 1 => 2, :coords => { 0 => 3, 1 => 4 }, 2 => 5, :foo => 'bar', 3 => { 0 => 10, 1 => 20 } })
              expect(parse('{coordinates={21,33}}')).to eq({ :coordinates => { 0 => 21, 1 => 33 } })
              expect(parse('{{id=1,price=5.0},{id=2,price=1.3}}')).to eq({ 0 => { :id => 1, :price => 5.0 }, 1 => { :id => 2, :price => 1.3 } })
            end

          end

        end

        context 'invalid tables' do

          it 'should not parse tables with missing commas' do
            expect(parse('{foo="bar"baz="boo"}')).to be_nil
          end

          it 'should not parse tables which are missing the leading or trailing bracket' do
            expect(parse('{foo="bar"')).to be_nil
            expect(parse('foo="bar"}')).to be_nil
          end

          it 'should not parse tables which have too many commas' do
            expect(parse('{,}')).to be_nil
            expect(parse('{,foo="bar"}')).to be_nil
            expect(parse('{foo="bar",}')).to be_nil
          end

        end

      end

    end

    context 'dumping' do

      it 'should dump array-like tables with integer indices' do
        expect({ 0 => 1, 1 => 2, 2 => 3, 3 => 4 }.to_lua).to eq '{1,2,3,4}'
        expect({ 0 => 1, 1 => { 0 => 1.1, 1 => 1.2 }, 2 => 2 }.to_lua).to eq '{1,{1.1,1.2},2}'
        expect({ 0 => 'Switzerland', 1 => 'Germany', 2 => 'France' }.to_lua).to eq '{"Switzerland","Germany","France"}'
        expect({ 0 => false, 1 => false, 2 => true, 3 => false }.to_lua).to eq '{false,false,true,false}'
      end

      it 'should dump array-like tables with mixed indices' do
        expect({ 0 => 'a', :'[1]' => 5, 1 => 6, :'[5]' => 'bar' }.to_lua).to eq '{"a",[1]=5,6,[5]="bar"}'
      end

      it 'should dump hash-like tables' do
        expect({ :name => 'John', :age => 25, :salary => 20.5, :on_fire => true }.to_lua).to eq '{name="John",age=25,salary=20.5,on_fire=true}'
        expect({ :name => { :first_name => 'John', :last_name => 'Madden' } }.to_lua).to eq '{name={first_name="John",last_name="Madden"}}'
      end

      it 'should dump mixed tables' do
        expect({ :pos => 'relative', 0 => 10, 1 => 5, 2 => 20 }.to_lua).to eq '{pos="relative",10,5,20}'
        expect({ 0 => 1, 1 => 2, :coords => { 0 => 3, 1 => 4 }, 2 => 5, :foo => 'bar', 3 => { 0 => 10, 1 => 20 } }.to_lua).to eq '{1,2,coords={3,4},5,foo="bar",{10,20}}'
        expect({ :coordinates => { 0 => 21, 1 => 33 } }.to_lua).to eq '{coordinates={21,33}}'
        expect({ 0 => { :id => 1, :price => 5.0 }, 1 => { :id => 2, :price => 1.3 } }.to_lua).to eq '{{id=1,price=5.0},{id=2,price=1.3}}'
      end

    end

  end

end
