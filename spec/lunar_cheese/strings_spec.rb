#coding: utf-8

require_relative '../spec_helper'

module LunarCheese

  describe LuaTableDumpParser do
    let(:p) { LuaTableDumpParser.new }

    context 'parsing' do
      before(:each) { p.root = :string }

      context 'valid strings' do

        it 'should parse empty strings' do
          expect(parse('""')).to eq ''
        end

        it 'should parse alphanumeric one-word strings' do
          expect(parse('"foobar"')).to eq 'foobar'
          expect(parse('"Michael"')).to eq 'Michael'
          expect(parse('"Parsing4Life"')).to eq 'Parsing4Life'
        end

        it 'should parse non-alphanumeric strings' do
          expect(parse('"@¼|¬§éLA12^"')).to eq '@¼|¬§éLA12^'
          expect(parse('"l\'église"')).to eq "l'église"
        end

        it 'should parse multi-word strings' do
          expect(parse('"Hello world, how are you today?"')).to eq 'Hello world, how are you today?'
          expect(parse('"e = m * c ^ 2"')).to eq 'e = m * c ^ 2'
        end

        it 'should parse strings with escaped quotation marks' do
          expect(parse('"He called her \"Honeymoon\""')).to eq 'He called her "Honeymoon"'
          expect(parse('"Yes, \"John\" is his name, now kill him!"')).to eq 'Yes, "John" is his name, now kill him!'
        end

      end

      context 'invalid strings' do

        it 'should not parse strings without quotation marks' do
          expect(parse('hello there')).to be_nil
          expect(parse('but I want to be parsed D:')).to be_nil
        end

        it 'should not parse strings with the leading or trailing quotation mark missing' do
          expect(parse('"Hello wor...')).to be_nil
          expect(parse('..lo world"')).to be_nil
        end

        it 'should not parse strings with non-escaped quotation marks' do
          expect(parse('"He called her \"Honeymoon""')).to be_nil
          expect(parse('"Yes, "John\" is his name, now kill him!"')).to be_nil
        end

      end

    end

    context 'dumping' do

      it 'should dump empty strings' do
        expect(''.to_lua).to eq '""'
      end

      it 'should dump strings' do
        expect('Hello World!'.to_lua).to eq '"Hello World!"'
      end

      it 'should escape quotation marks' do
        expect('He called her "Honeymoon"'.to_lua).to eq '"He called her \"Honeymoon\""'
      end

    end

  end

end
