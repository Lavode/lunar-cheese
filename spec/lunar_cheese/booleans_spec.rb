#coding: utf-8

require_relative '../spec_helper'

module LunarCheese

  describe LuaTableDumpParser do
    let(:p) { LuaTableDumpParser.new }

    context 'parsing' do
      before(:each) { p.root = :boolean }

      it 'should parse booleans' do
        result = parse('false')
        expect(result).to be_false
        expect(result).to_not be_nil

        expect(parse('true')).to be_true
      end

      it 'should not parse invalid booleans' do
        expect(parse('False')).to be_nil
        expect(parse('True')).to be_nil
        expect(parse('fAtrue')).to be_nil
      end

    end

    context 'dumping' do

      it 'should dump booleans' do
        expect(false.to_lua).to eq 'false'
        expect(true.to_lua).to eq 'true'
      end

    end

  end

end
